from app import app
import time
from flask import Flask, request, url_for, jsonify, \
    render_template, session, redirect
from flask_socketio import SocketIO, emit, join_room, \
    leave_room, close_room, rooms, disconnect
from flask_session import Session


socketio = SocketIO(app)

users = []
rooms = ['public']
user_room = []
chat_storage = []
@app.route("/", methods=['GET', 'POST'])
def index():
    if request.method == 'POST':
        username = request.form['username']
        if username in users:
            return render_template('error.html', title='Flack Chat', error="User already exists")
        session['user'] = username
        users.append(username)
        return redirect(url_for('user_index', user=session['user'])), 302
    if 'user' in session:
        return redirect(url_for('user_index', user=session['user'])), 302
    return render_template('index.html', title='Flack Chat')

# for users
@app.route("/user/<user>")
def user_index(user):
    if not('user' in session):
        return redirect(url_for('index')), 302
    if user is None:
        return redirect(url_for('index')), 302
    if user in session['user']:
        if len(user_room) == 1:
            return redirect(url_for('chat', user=user, room=user_room[0]))
        return render_template('user.html', title='Flack Chat', username=user, rooms=rooms)


# for room
@app.route("/create/room", methods=['POST'])
def create_room():
    if request.method == 'POST':
        room = request.form['room_name']
        if len(room) < 4:
            return jsonify({"type": "error", "message": f"Room must be higher to 4 letters"})
        if room in rooms:
            return jsonify({"type": "error", "message": f"Room already exists"})
        rooms.append(room)
        return jsonify({"type": "success", "message": f"Room {room} created", "room": room})


@app.route("/<user>/<room>")
def chat(user, room):
    if user is None or not(user in users):
        return "No user"
    if room is None or not(room in rooms):
        return "No room"
    # add user in room
    if len(user_room) == 0:
        user_room.append(room)
    return render_template("chat.html", username=user, room=room, chats = chat_storage, rooms = rooms)
# logout
@app.route("/logout")
def logout():
    if len(users) > 0:
        users.remove(session['user'])
    session.clear()
    return redirect("/", code=302)

# socketIO
@socketio.on('message')
def message(data):
    message_rec = data['message']
    room_message = data['room']
    user_message = data['user']
    # return message on room
    emit('message_sent', {'m': message_rec, 'r': room_message,
                          'u': user_message, 't': time.strftime("%H:%M:%S")}, room=room_message, broadcast=True)
    # save on server storage
    limitMessages({'m': message_rec, 'r': room_message, 'u': user_message, 't': time.strftime("%H:%M:%S")}, chat_storage)
# join room


@socketio.on('join')
def join(data):
    room = data['room']
    join_room(room)
    emit('joined', {'room': room, 'username': session['user']})

@socketio.on('leave')
def leave(data):
    room = data['room']
    leave_room(room)
    if room in user_room:
        user_room.remove(room)
    emit('leaved', { 'room': room, 'username': session['user'] })
# if any user enter sent message
@socketio.on('event_room')
def message_event(data):
    emit("received_event", {
         "message": data['message'], "username": session['user'], 'time': time.strftime("%H:%M:%S")}, room=data['room'], broadcast=True)
# 404
@app.errorhandler(404)
def page_not_found(e):
    # note that we set the 404 status explicitly
    return render_template('error.html', title="Flack Chat", error="404 :("), 404

# # # LIMIT MESSAGES IN SERVER STORAGE


def limitMessages(message, list):
    if len(list) <= 100:
        list.append(message)
    return list
