import os

from flask import  Flask, request, url_for, jsonify, \
    render_template, session
from flask_socketio import SocketIO, emit, join_room, \
    leave_room, close_room, rooms, disconnect
from flask_session import Session

app = Flask(__name__)
app.config["SECRET_KEY"] = os.getenv("SECRET_KEY")
# Configure session to use filesystem
app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"

Session(app)



#index controller
from controllers.index import *

# Run python app.pu --no-reload
if __name__ == "__main__":
    socketio.run(app, host='0.0.0.0', debug=True)