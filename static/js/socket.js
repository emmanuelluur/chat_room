document.addEventListener('DOMContentLoaded', () => {
    const socket = io.connect(location.protocol + '//' + document.domain + ':' + location.port);
    const btn_send_message = document.getElementById('send-message');
    let user = document.getElementById('user-data').value;
    let room = document.getElementById('room-actual').value;
    const leave = document.getElementById('leave');
    const logout = document.getElementById('logout');
    // for chat in any room 
    // need join
    socket.emit('join', {
        'room': room
    })
    btn_send_message.addEventListener('click', () => {
        // emit message
        let message = document.getElementById('message');

        socket.emit('message', {
            'message': message.value,
            'room': room,
            'user': user
        })
        message.value = '';
        message.focus();
    })


    socket.on('message_sent', data => {


        let media_container = document.createElement('div');
        media_container.setAttribute('class', 'container mt-3');
        let media_border = document.createElement('div');
        media_border.setAttribute('class', 'media border p-3');
        let media_body = document.createElement('div');
        media_body.setAttribute('class', 'media-body');

        let head = document.createElement('strong');
        let small = document.createElement('small');
        let iC = document.createElement('i');
        let pa = document.createElement('p');
        let txtP = document.createTextNode(data['m']);
        let txtHead = document.createTextNode(data['u']);
        let txtI = document.createTextNode(data['t']);

        pa.appendChild(txtP);
        iC.appendChild(txtI);
        small.appendChild(iC);
        head.appendChild(txtHead);
        head.appendChild(small);

        media_body.appendChild(head);
        media_body.appendChild(pa);

        media_border.appendChild(media_body);
        media_container.appendChild(media_border);

        document.getElementById('_messages').appendChild(media_container);

    });

    leave.addEventListener('click', () => {
        socket.emit('leave', {
            'room':room
        })
    });

    logout.addEventListener('click', () => {
        socket.emit('leave', {
            'room':room
        })
    });
    socket.on('joined', data => {
        socket.emit('event_room', {
            'message': `${data['username']} join to ${data['room']}`,
            'room': room
        })
    });
    socket.on('leaved', data => {
        socket.emit('event_room', {
            'message': `${data['username']} leave to ${data['room']}`,
            'room': room
        })
        setTimeout(function () {
            location.assign('/')
        }, 1000);
    });
    socket.on('received_event', data => {
        document.getElementById('events_user').innerHTML = `${data['message']}`;
    });
})